
<?php include 'includes/header.php'; ?>
 
 <div id="page">   
<div class="header header-fixed header-logo-app">
        <a href="#" class="header-title">TABLERO</a>
        <a href="#" class="header-icon header-icon-2" data-menu="menu-1"><i class="fas fa-bars"></i></a>
        <a href="mailto:soporte@sumapp.cloud?Subject=Tengo un problema" class="header-icon header-icon-3"><i class="fa fa-envelope"></i></a>
        <a href="#" class="header-icon header-icon-4" data-toggle-theme><i class="fas fa-moon"></i></a>
	</div>
	<?php include('includes/menu.php');?>
	<div class="page-content header-clear-large">	   
        
        <div class="profile-header">
            <div class="profile-left">
                <h1>
                    <?php echo $result->designation ?>
                    <div class="date">
                <span id="diaSemana" class="weekDay"></span>, 
                <span id="dia" class="day"></span> de
                <span id="mes" class="month"></span> del
                <span id="anio" class="year"></span>
            </div>
            <div class="clock">
                <span id="horas" class="hours"></span> :
                <span id="minutos" class="minutes"></span> :
                <span id="segundos" class="seconds"></span>
            </div>
                </h1>
    <?php      $sqlformularios="SELECT b.c_nombre_bloque AS Bloque,count(*) Total FROM (SELECT * FROM `tb_respuesta` WHERE usuario='".$_SESSION['alogin']."' AND DAY(SUBSTR(fecha,1,10))=DAY(NOW()) GROUP BY clave_registro) t 
INNER JOIN tb_encuesta_bloque b ON b.id_bloque=t.idbloque
GROUP BY idbloque ";


$queryformularios = $conexion->query($sqlformularios);//Se ejecuta consulta
$arrayformularios= array(); // Array donde vamos a guardar los datos 
while($resultadoformularios = $queryformularios->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arrayformularios[]=$resultadoformularios; // Guardar los resultados en la variable

} 

foreach ($arrayformularios as $formularios) {
$totalformularios=count($formularios->Bloque);
}

?>

<br>
<center><h2>Resueltos Hoy</h2>
    <?php if ($totalformularios>0) { ?>
<?php foreach ($arrayformularios as $formularios): ?>
    <h3><?php echo $formularios->Bloque ?>:&nbsp;<span><?php echo $formularios->Total ?></span></h3>
<?php endforeach ?>
<br></center>
<?php }else{ ?>
        <h3>Ninguno</h3>
        <br>
    <?php } ?>
                <div class="clear"></div>
            </div>
            <div class="profile-right">
                <a href="#">
       
                	
               
                    
                </a>
            </div>
        </div>
        <center>
        <?php
		$sqlinspeccionmes = "SELECT 'ENE' AS MES ,count(*) AS TOTAL FROM limpieza WHERE MONTH(fechaCheck)=1 AND SUBSTR(fechaCheck,1,4)=YEAR(NOW()) AND hotel='$result->designation' 
UNION ALL
SELECT 'FEB',count(*) FROM limpieza WHERE MONTH(fechaCheck)=2 AND SUBSTR(fechaCheck,1,4)=YEAR(NOW()) AND hotel='$result->designation' 
UNION ALL
SELECT 'MAR',count(*) FROM limpieza WHERE MONTH(fechaCheck)=3 AND SUBSTR(fechaCheck,1,4)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'ABR',count(*) FROM limpieza WHERE MONTH(fechaCheck)=4 AND SUBSTR(fechaCheck,1,4)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'MAY',count(*) FROM limpieza WHERE MONTH(fechaCheck)=5 AND SUBSTR(fechaCheck,1,4)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'JUN',count(*) FROM limpieza WHERE MONTH(fechaCheck)=6 AND SUBSTR(fechaCheck,1,4)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'JUL',count(*) FROM limpieza WHERE MONTH(fechaCheck)=7 AND SUBSTR(fechaCheck,1,4)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'AGO',count(*) FROM limpieza WHERE MONTH(fechaCheck)=8 AND SUBSTR(fechaCheck,1,4)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'SEP',count(*) FROM limpieza WHERE MONTH(fechaCheck)=9 AND SUBSTR(fechaCheck,1,4)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'OCT',count(*) FROM limpieza WHERE MONTH(fechaCheck)=10 AND SUBSTR(fechaCheck,1,4)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'NOV',count(*) FROM limpieza WHERE MONTH(fechaCheck)=11 AND SUBSTR(fechaCheck,1,4)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'DIC',count(*) FROM limpieza WHERE MONTH(fechaCheck)=12 AND SUBSTR(fechaCheck,1,4)=YEAR(NOW()) AND hotel='$result->designation'";
$sqlnoconformidad="SELECT 'ENE' AS MES,
SUM((SELECT
CASE
 WHEN puertaLimpia = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN paredTechoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN pisoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN espejoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN tvMueblesSinPolvo = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN funcionamientoTV = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN almohadaExtra = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN cortinasLimpias= 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN iluminacionSinPol = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN camaBlancosLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN bolsaListaLavande = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN indicadorVoltaje = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN dispensadorPapel = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN lavaboGrifos = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN plafonesLimpios = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN toallasSinManchas= 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN wcLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN puertaCristal= 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN rejunteLimpio = 'No' THEN 1 ELSE 0
END
)
) AS TOTAL
FROM limpieza WHERE MONTH(fechaCheck)=1 AND SUBSTR(fechaCheck,1,4)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'FEB' AS MES,
SUM((SELECT
CASE
 WHEN puertaLimpia = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN paredTechoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN pisoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN espejoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN tvMueblesSinPolvo = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN funcionamientoTV = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN almohadaExtra = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN cortinasLimpias= 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN iluminacionSinPol = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN camaBlancosLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN bolsaListaLavande = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN indicadorVoltaje = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN dispensadorPapel = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN lavaboGrifos = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN plafonesLimpios = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN toallasSinManchas= 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN wcLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN puertaCristal= 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN rejunteLimpio = 'No' THEN 1 ELSE 0
END
)
) AS TOTAL
FROM limpieza WHERE MONTH(fechaCheck)=2 AND SUBSTR(fechaCheck,1,4)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'MAR' AS MES,
SUM((SELECT
CASE
 WHEN puertaLimpia = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN paredTechoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN pisoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN espejoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN tvMueblesSinPolvo = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN funcionamientoTV = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN almohadaExtra = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN cortinasLimpias= 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN iluminacionSinPol = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN camaBlancosLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN bolsaListaLavande = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN indicadorVoltaje = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN dispensadorPapel = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN lavaboGrifos = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN plafonesLimpios = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN toallasSinManchas= 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN wcLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN puertaCristal= 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN rejunteLimpio = 'No' THEN 1 ELSE 0
END
)
) AS TOTAL
FROM limpieza WHERE MONTH(fechaCheck)=3 AND SUBSTR(fechaCheck,1,4)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'ABR' AS MES,
SUM((SELECT
CASE
 WHEN puertaLimpia = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN paredTechoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN pisoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN espejoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN tvMueblesSinPolvo = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN funcionamientoTV = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN almohadaExtra = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN cortinasLimpias= 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN iluminacionSinPol = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN camaBlancosLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN bolsaListaLavande = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN indicadorVoltaje = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN dispensadorPapel = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN lavaboGrifos = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN plafonesLimpios = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN toallasSinManchas= 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN wcLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN puertaCristal= 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN rejunteLimpio = 'No' THEN 1 ELSE 0
END
)
) AS TOTAL
FROM limpieza WHERE MONTH(fechaCheck)=4 AND SUBSTR(fechaCheck,1,4)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'MAY' AS MES,
SUM((SELECT
CASE
 WHEN puertaLimpia = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN paredTechoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN pisoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN espejoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN tvMueblesSinPolvo = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN funcionamientoTV = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN almohadaExtra = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN cortinasLimpias= 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN iluminacionSinPol = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN camaBlancosLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN bolsaListaLavande = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN indicadorVoltaje = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN dispensadorPapel = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN lavaboGrifos = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN plafonesLimpios = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN toallasSinManchas= 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN wcLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN puertaCristal= 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN rejunteLimpio = 'No' THEN 1 ELSE 0
END
)
) AS TOTAL
FROM limpieza WHERE MONTH(fechaCheck)=5 AND SUBSTR(fechaCheck,1,4)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'JUN' AS MES,
SUM((SELECT
CASE
 WHEN puertaLimpia = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN paredTechoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN pisoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN espejoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN tvMueblesSinPolvo = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN funcionamientoTV = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN almohadaExtra = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN cortinasLimpias= 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN iluminacionSinPol = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN camaBlancosLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN bolsaListaLavande = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN indicadorVoltaje = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN dispensadorPapel = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN lavaboGrifos = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN plafonesLimpios = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN toallasSinManchas= 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN wcLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN puertaCristal= 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN rejunteLimpio = 'No' THEN 1 ELSE 0
END
)
) AS TOTAL
FROM limpieza WHERE MONTH(fechaCheck)=6 AND SUBSTR(fechaCheck,1,4)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'JUL' AS MES,
SUM((SELECT
CASE
 WHEN puertaLimpia = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN paredTechoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN pisoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN espejoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN tvMueblesSinPolvo = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN funcionamientoTV = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN almohadaExtra = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN cortinasLimpias= 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN iluminacionSinPol = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN camaBlancosLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN bolsaListaLavande = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN indicadorVoltaje = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN dispensadorPapel = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN lavaboGrifos = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN plafonesLimpios = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN toallasSinManchas= 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN wcLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN puertaCristal= 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN rejunteLimpio = 'No' THEN 1 ELSE 0
END
)
) AS TOTAL
FROM limpieza WHERE MONTH(fechaCheck)=7 AND SUBSTR(fechaCheck,1,4)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'AGO' AS MES,
SUM((SELECT
CASE
 WHEN puertaLimpia = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN paredTechoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN pisoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN espejoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN tvMueblesSinPolvo = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN funcionamientoTV = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN almohadaExtra = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN cortinasLimpias= 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN iluminacionSinPol = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN camaBlancosLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN bolsaListaLavande = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN indicadorVoltaje = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN dispensadorPapel = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN lavaboGrifos = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN plafonesLimpios = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN toallasSinManchas= 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN wcLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN puertaCristal= 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN rejunteLimpio = 'No' THEN 1 ELSE 0
END
)
) AS TOTAL
FROM limpieza WHERE MONTH(fechaCheck)=8 AND SUBSTR(fechaCheck,1,4)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'SEP' AS MES,
SUM((SELECT
CASE
 WHEN puertaLimpia = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN paredTechoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN pisoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN espejoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN tvMueblesSinPolvo = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN funcionamientoTV = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN almohadaExtra = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN cortinasLimpias= 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN iluminacionSinPol = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN camaBlancosLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN bolsaListaLavande = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN indicadorVoltaje = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN dispensadorPapel = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN lavaboGrifos = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN plafonesLimpios = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN toallasSinManchas= 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN wcLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN puertaCristal= 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN rejunteLimpio = 'No' THEN 1 ELSE 0
END
)
) AS TOTAL
FROM limpieza WHERE MONTH(fechaCheck)=9 AND SUBSTR(fechaCheck,1,4)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'OCT' AS MES,
SUM((SELECT
CASE
 WHEN puertaLimpia = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN paredTechoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN pisoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN espejoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN tvMueblesSinPolvo = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN funcionamientoTV = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN almohadaExtra = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN cortinasLimpias= 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN iluminacionSinPol = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN camaBlancosLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN bolsaListaLavande = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN indicadorVoltaje = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN dispensadorPapel = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN lavaboGrifos = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN plafonesLimpios = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN toallasSinManchas= 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN wcLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN puertaCristal= 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN rejunteLimpio = 'No' THEN 1 ELSE 0
END
)
) AS TOTAL
FROM limpieza WHERE MONTH(fechaCheck)=10 AND SUBSTR(fechaCheck,1,4)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'NOV' AS MES,
SUM((SELECT
CASE
 WHEN puertaLimpia = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN paredTechoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN pisoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN espejoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN tvMueblesSinPolvo = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN funcionamientoTV = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN almohadaExtra = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN cortinasLimpias= 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN iluminacionSinPol = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN camaBlancosLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN bolsaListaLavande = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN indicadorVoltaje = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN dispensadorPapel = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN lavaboGrifos = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN plafonesLimpios = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN toallasSinManchas= 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN wcLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN puertaCristal= 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN rejunteLimpio = 'No' THEN 1 ELSE 0
END
)
) AS TOTAL
FROM limpieza WHERE MONTH(fechaCheck)=11 AND SUBSTR(fechaCheck,1,4)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'DIC' AS MES,
SUM((SELECT
CASE
 WHEN puertaLimpia = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN paredTechoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN pisoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN espejoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN tvMueblesSinPolvo = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN funcionamientoTV = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN almohadaExtra = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN cortinasLimpias= 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN iluminacionSinPol = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN camaBlancosLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN bolsaListaLavande = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN indicadorVoltaje = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN dispensadorPapel = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN lavaboGrifos = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN plafonesLimpios = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN toallasSinManchas= 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN wcLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN puertaCristal= 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN rejunteLimpio = 'No' THEN 1 ELSE 0
END
)
) AS TOTAL
FROM limpieza WHERE MONTH(fechaCheck)=12 AND SUBSTR(fechaCheck,1,4)=YEAR(NOW()) AND hotel='$result->designation'";
$sqlnoconformidadcamarista="SELECT camarista,
SUM((SELECT
CASE
 WHEN puertaLimpia = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN paredTechoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN pisoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN espejoLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN tvMueblesSinPolvo = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN funcionamientoTV = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN almohadaExtra = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN cortinasLimpias= 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN iluminacionSinPol = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN camaBlancosLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN bolsaListaLavande = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN indicadorVoltaje = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN dispensadorPapel = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN lavaboGrifos = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN plafonesLimpios = 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN toallasSinManchas= 'No' THEN 1 ELSE 0
END
)+
(SELECT
CASE
 WHEN wcLimpio = 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN puertaCristal= 'No' THEN 1 ELSE 0
END
)
+
(SELECT
CASE
 WHEN rejunteLimpio = 'No' THEN 1 ELSE 0
END
)
) AS TOTAL
FROM limpieza WHERE MONTH(fechaCheck)=MONTH(NOW()) AND SUBSTR(fechaCheck,1,4)=YEAR(NOW()) AND hotel='$result->designation' GROUP BY camarista";
$sqlinspeccionsupervisor="SELECT email ,count(*) AS TOTAL FROM limpieza WHERE MONTH(fechaCheck)=MONTH(NOW()) AND SUBSTR(fechaCheck,1,4)=YEAR(NOW()) AND hotel='$result->designation' GROUP BY email";
$sqlpnoconformidad="SELECT 'P1' AS PREGUNTA,COUNT(puertaLimpia) AS TOTAL
 from limpieza WHERE puertaLimpia='No' AND MONTH(fechaCheck)=MONTH(NOW()) AND YEAR(fechaCheck)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'P2', COUNT(paredTechoLimpio) 
 from limpieza WHERE paredTechoLimpio='No' AND MONTH(fechaCheck)=MONTH(NOW()) AND YEAR(fechaCheck)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'P3', COUNT(pisoLimpio) 
 from limpieza WHERE pisoLimpio='No' AND MONTH(fechaCheck)=MONTH(NOW()) AND YEAR(fechaCheck)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'P4', COUNT(espejoLimpio) 
 from limpieza WHERE espejoLimpio='No' AND MONTH(fechaCheck)=MONTH(NOW()) AND YEAR(fechaCheck)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'P5', COUNT(tvMueblesSinPolvo) 
 from limpieza WHERE tvMueblesSinPolvo='No' AND MONTH(fechaCheck)=MONTH(NOW()) AND YEAR(fechaCheck)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'P6', COUNT(funcionamientoTV) 
 from limpieza WHERE funcionamientoTV='No' AND MONTH(fechaCheck)=MONTH(NOW()) AND YEAR(fechaCheck)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'P7', COUNT(almohadaExtra) 
 from limpieza WHERE almohadaExtra='No' AND MONTH(fechaCheck)=MONTH(NOW()) AND YEAR(fechaCheck)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'P8', COUNT(cortinasLimpias) 
 from limpieza WHERE cortinasLimpias='No' AND MONTH(fechaCheck)=MONTH(NOW()) AND YEAR(fechaCheck)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'P9', COUNT(iluminacionSinPol) 
 from limpieza WHERE iluminacionSinPol='No' AND MONTH(fechaCheck)=MONTH(NOW()) AND YEAR(fechaCheck)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'P10', COUNT(camaBlancosLimpio) 
 from limpieza WHERE camaBlancosLimpio='No' AND MONTH(fechaCheck)=MONTH(NOW()) AND YEAR(fechaCheck)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'P11', COUNT(bolsaListaLavande) 
 from limpieza WHERE bolsaListaLavande='No' AND MONTH(fechaCheck)=MONTH(NOW()) AND YEAR(fechaCheck)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'P12', COUNT(indicadorVoltaje) 
 from limpieza WHERE indicadorVoltaje='No' AND MONTH(fechaCheck)=MONTH(NOW()) AND YEAR(fechaCheck)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'P13', COUNT(dispensadorPapel) 
 from limpieza WHERE dispensadorPapel='No' AND MONTH(fechaCheck)=MONTH(NOW()) AND YEAR(fechaCheck)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'P14', COUNT(lavaboGrifos) 
 from limpieza WHERE lavaboGrifos='No' AND MONTH(fechaCheck)=MONTH(NOW()) AND YEAR(fechaCheck)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'P15', COUNT(plafonesLimpios) 
 from limpieza WHERE plafonesLimpios='No' AND MONTH(fechaCheck)=MONTH(NOW()) AND YEAR(fechaCheck)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'P16', COUNT(toallasSinManchas) 
 from limpieza WHERE toallasSinManchas='No' AND MONTH(fechaCheck)=MONTH(NOW()) AND YEAR(fechaCheck)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'P17', COUNT(wcLimpio) 
 from limpieza WHERE wcLimpio='No' AND MONTH(fechaCheck)=MONTH(NOW()) AND YEAR(fechaCheck)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'P18', COUNT(puertaCristal) 
 from limpieza WHERE puertaCristal='No' AND MONTH(fechaCheck)=MONTH(NOW()) AND YEAR(fechaCheck)=YEAR(NOW()) AND hotel='$result->designation'
UNION ALL
SELECT 'P19', COUNT(rejunteLimpio) 
 from limpieza WHERE rejunteLimpio='No' AND MONTH(fechaCheck)=MONTH(NOW()) AND YEAR(fechaCheck)=YEAR(NOW()) AND hotel='$result->designation'";
		$queryinspeccionmes = $dbh -> prepare($sqlinspeccionmes);
      $querynoconformidad = $dbh -> prepare($sqlnoconformidad);
       $querynoconformidadcamarista = $dbh -> prepare($sqlnoconformidadcamarista);
         $queryinspeccionsupervisor = $dbh -> prepare($sqlinspeccionsupervisor);
         $querypnoconformidad = $dbh -> prepare($sqlpnoconformidad);
		$queryinspeccionmes->execute();
    $querynoconformidad->execute();
    $querynoconformidadcamarista->execute();
    $queryinspeccionsupervisor->execute();
    $querypnoconformidad->execute();
		while($resultadoinspeccionmes = $queryinspeccionmes->fetch(PDO::FETCH_OBJ)){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arrayinspeccionmes[]=$resultadoinspeccionmes; // Guardar los resultados en la variable
}
    while($resultadonoconformidad = $querynoconformidad->fetch(PDO::FETCH_OBJ)){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arraynoconformidad[]=$resultadonoconformidad; // Guardar los resultados en la variable
}
    while($resultadonoconformidadcamarista = $querynoconformidadcamarista->fetch(PDO::FETCH_OBJ)){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arraynoconformidadcamarista[]=$resultadonoconformidadcamarista; // Guardar los resultados en la variable
}
    while($resultadoinspeccionsupervisor = $queryinspeccionsupervisor->fetch(PDO::FETCH_OBJ)){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arrayinspeccionsupervisor[]=$resultadoinspeccionsupervisor; // Guardar los resultados en la variable
}
    while($resultadopnoconformidad = $querypnoconformidad->fetch(PDO::FETCH_OBJ)){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arraypnoconformidad[]=$resultadopnoconformidad; // Guardar los resultados en la variable
}
?>	

 <!--<h1 class="content">
 	<h2>Graficas</h2>
           Resultados de informacion en graficos
<p>	La informacion que se muestran en las graficas es capturada por el gerente del hotel.</p>


        </h1>
        </center>
        <div class="divider divider-margins"></div>
        
		<div class="content">

			<h2 class="center-text bolder bottom-0">Inspeccion por mes</h2>
			<p class="center-text under-heading font-12 color-highlight bottom-15">Año <?php echo $ano ?></p>
			<div class="content bottom-0">
				<canvas id="inspeccionmes" ></canvas>
			</div>
		</div>
     <div class="divider divider-margins"></div>
        
    <div class="content">
      <h2 class="center-text bolder bottom-0">No conformidad</h2>
      <p class="center-text under-heading font-12 color-highlight bottom-15">Año <?php echo $ano ?></p>
      <div class="content bottom-0">
        <canvas id="Noconformidades" height="200" ></canvas>
      </div>
    </div>
     <div class="divider divider-margins"></div>
         <div class="content">
      <h2 class="center-text bolder bottom-0">No conformidad por pregunta</h2>
      <p class="center-text under-heading font-12 color-highlight bottom-15" >Mes en curso</p>
      <div class="content bottom-0">
        <canvas id="incidenciasporpregunta" height="200" ></canvas>
      </div>
    </div>
        <div class="divider divider-margins"></div>
         <div class="content">
      <h2 class="center-text bolder bottom-0">No conformidad por camarista</h2>
      <p class="center-text under-heading font-12 color-highlight bottom-15" >Mes en curso</p>
      <div class="content bottom-0">
        <canvas id="Noconformidadescamarista" height="200" ></canvas>
      </div>
    </div>
       <div class="divider divider-margins"></div>
         <div class="content">
      <h2 class="center-text bolder bottom-0">Inspeccion supervisor</h2>
      <p class="center-text under-heading font-12 color-highlight bottom-15" >Mes en curso</p>
      <div class="content bottom-0">
        <canvas id="Inspeccionsupervisor" height="200" ></canvas>
      </div>
    </div>-->
   </div>
</div>

<?php include('includes/footer.php');?>
</body>
</html>

<script type="text/javascript">
  $(function(){
  var actualizarHora = function(){
    var fecha = new Date(),
        hora = fecha.getHours(),
        minutos = fecha.getMinutes(),
        segundos = fecha.getSeconds(),
        diaSemana = fecha.getDay(),
        dia = fecha.getDate(),
        mes = fecha.getMonth(),
        anio = fecha.getFullYear(),
        ampm;
    
    var $pHoras = $("#horas"),
        $pSegundos = $("#segundos"),
        $pMinutos = $("#minutos"),
        $pAMPM = $("#ampm"),
        $pDiaSemana = $("#diaSemana"),
        $pDia = $("#dia"),
        $pMes = $("#mes"),
        $pAnio = $("#anio");
    var semana = ['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'];
    var meses = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
    
    $pDiaSemana.text(semana[diaSemana]);
    $pDia.text(dia);
    $pMes.text(meses[mes]);
    $pAnio.text(anio);
    if(hora>=12){
      hora = hora - 12;
      ampm = "PM";
    }else{
      ampm = "AM";
    }
    if(hora == 0){
      hora = 12;
    }
    if(hora<10){$pHoras.text("0"+hora)}else{$pHoras.text(hora)};
    if(minutos<10){$pMinutos.text("0"+minutos)}else{$pMinutos.text(minutos)};
    if(segundos<10){$pSegundos.text("0"+segundos)}else{$pSegundos.text(segundos)};
    $pAMPM.text(ampm);
    
  };
  
  
  actualizarHora();
  var intervalo = setInterval(actualizarHora,1000);
});
</script>