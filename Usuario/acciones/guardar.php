<?php

header("Content-Type: text/html;charset=utf-8");
date_default_timezone_set("America/Mexico_City");
include('../conexion.php');
require "PHPMailer/Exception.php";
require "PHPMailer/PHPMailer.php";
require "PHPMailer/SMTP.php";
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require_once("libs/dompdf/dompdf_config.inc.php");



$fecha= date("Y-m-d");
$hora=date('h:i A');
$usuario=$_POST["usuario"];
$longitud=$_POST["longitud"];
$latitud=$_POST["latitud"];
$cuestionario=$_POST["cuestionario"];
$sucursal=$_POST["sucursal"];
$bloque=$_POST["bloque"];

$sqlcarpeta="SELECT A.carpeta FROM tb_encuesta C
INNER JOIN tb_app A ON A.id_app=C.id_app
WHERE C.id_encuesta='".$cuestionario."'
";

$querycarpeta = $conexion->query($sqlcarpeta);//Se ejecuta consulta
$arraycarpeta= array(); // Array donde vamos a guardar los datos 
while($resultadocarpeta = $querycarpeta->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arraycarpeta[]=$resultadocarpeta; // Guardar los resultados en la variable
}

foreach ($arraycarpeta as $carp ) :
 $carpetafotos=$carp->carpeta;
endforeach;

$pregunta=$_POST["pregunta"];
$respuesta=$_POST["respuesta"];
$comentario=$_POST["comentario"];
$prefijo = substr(md5(uniqid(rand())),0,6);
$carpeta='/var/www/vhosts/sumapp.cloud/fotos.sumapp.cloud/Sumapp/'.$carpetafotos.'';
$evidencias= $_FILES['evidencia'];    
$nombrearchivo = $_FILES["evidencia"]['name'];



$cadena="INSERT INTO tb_respuesta (usuario, longitud, latitud, idcuestionario, idbloque, idpregunta, sucursal, respuesta, evidencia,comentario,clave_registro, fecha) VALUES";

for ($i=0; $i <count($pregunta) ; $i++) { 
	if (filesize($evidencias['tmp_name'][$i])>0) {
 $nombreimagen[$i]=substr(md5(uniqid(rand($evidencias['name'][$i]))),0,6).'.jpg';
	$direccion.=file_put_contents($carpeta.'/'.$nombreimagen[$i].'', file_get_contents($evidencias['tmp_name'][$i]));

 
}
	$cadena.="('".$usuario."','".$longitud."','".$latitud."','".$cuestionario."','".$bloque."','".$pregunta[$i]."','".$sucursal."','".$respuesta[$i]."','".$nombreimagen[$i]."','".$comentario[$i]."','".$prefijo."','".$fecha."-".$hora."'),";

}

$cadena_final=substr($cadena, 0, -1);
$cadena_final.=";";

$res = $conexion->query($cadena_final);



$sqlincidencia="SELECT r.usuario,b.c_nombre_bloque,p.c_titulo_pregunta,r.respuesta,r.comentario FROM tb_respuesta r
INNER JOIN tb_encuesta_bloque b ON b.id_bloque=r.idbloque
INNER JOIN tb_encuesta_pregunta p ON p.id_pregunta=r.idpregunta
 WHERE r.respuesta='No' AND MONTH(SUBSTR(fecha,1,10))=MONTH(NOW())  AND YEAR(SUBSTR(fecha,1,10))=YEAR(NOW()) AND DAY(SUBSTR(fecha,1,10))=DAY(NOW()) AND r.usuario='".$usuario."' AND r.idbloque='".$bloque."'
";

$queryincidencia = $conexion->query($sqlincidencia);//Se ejecuta consulta
$arrayincidencia= array(); // Array donde vamos a guardar los datos 
while($resultadoincidencia = $queryincidencia->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arrayincidencia[]=$resultadoincidencia; // Guardar los resultados en la variable
}

$sqlusuario="SELECT email FROM tb_alertas WHERE cuenta='".$usuario."'";
$queryusuario = $conexion->query($sqlusuario);//Se ejecuta consulta
$arrayusuario= array(); // Array donde vamos a guardar los datos 
while($resultadousuario = $queryusuario->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arrayusuario[]=$resultadousuario; // Guardar los resultados en la variable
}

foreach ($arrayincidencia as $ci ) {
    $totalincidencias=$ci->c_titulo_pregunta;
}
foreach ($arrayincidencia as $bl ) {
	$bloqueres=$bl->c_nombre_bloque;
}




if (count($totalincidencias)>0) {


$pdf='<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Table</title>
</head>
<center>
 <h1>Irregularidades </h1><br>
 <h3>Seccion:&nbsp;'.$bloqueres.'</h3>
<h3>Usuario:&nbsp;'.$usuario.'</h3>
 </center>
   
    <table style="width: 700px; margin: 0 auto;"; border="1" cellpadding="1" cellspacing="1">
          <thead>
                                   
                                        <tr>
                                          <th>PREGUNTA</th>
                                        <th>RESPUESTA</th>
                                        <th>COMENTARIO</th>
                                        </tr>
                                    </thead><tbody>';
                                    foreach($arrayincidencia as $incidencia):
         $pdf.='
                                        <tr >
                              
                                            <td>'.$incidencia->c_titulo_pregunta.'</td>
                                            <td >'.$incidencia->respuesta.'</td> 
                                            <td >'.$incidencia->comentario.'</td> 
                                             
                                        </tr>
                                    ';   
                                    endforeach; 
                                     $pdf.='
       </tbody>
      </table>
      
      <br>
      <center><strong style="color:red;">Fecha:'.$hoy = date("Y-m-d H:i:s").'</strong></center>
<body>
</body>
</html>';

$codigo=utf8_encode($pdf);
$dompdf=new DOMPDF();
$dompdf->load_html($codigo);
ini_set("memory_limit","128M");
$dompdf->render();
$output = $dompdf->output();


$mail = new PHPMailer();
$mail->IsSMTP();
$mail->Mailer = "smtp";
$mail->Host = "mail.smtp2go.com";
$mail->Port = "2525"; // 8025, 587 and 25 can also be used. Use Port 465 for SSL.
$mail->SMTPAuth = true;
$mail->SMTPSecure = 'tls';
$mail->Username = "soporte@empresavirtual.mx";
$mail->Password = "laredo";

$mail->From = "no-reply@sumapp.cloud";
$mail->FromName = "ALERTA IRREGULARIDADES";
foreach ($arrayusuario as $usu ) {
$mail->AddAddress($usu->email, "Sumapp");
}


$mail->Subject = "ALERTA IRREGULARIDADES";

foreach ($arrayincidencia as $incidencia) :
$contemail.="
<tr>
<td>&nbsp;".$incidencia->c_titulo_pregunta."&nbsp;</td>
<td>&nbsp;".$incidencia->respuesta."&nbsp;</td>
<td>&nbsp;".$incidencia->comentario."&nbsp;</td>
</tr>
";
endforeach;
$mail->AddStringAttachment($output, 'Alerta_'.$hoy = date("Y-m-d").'.pdf', 'base64', 'application/pdf');
$mail->msgHTML('<!doctype html>
<html>
  <head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Simple Transactional Email</title>
    <style>
      /* -------------------------------------
          GLOBAL RESETS
      ------------------------------------- */
      
      /*All the styling goes here*/
      b{
        color:white;
      }
      img {
        border: none;
        -ms-interpolation-mode: bicubic;
        max-width: 100%; 
      }

      body {
        background-color: #f6f6f6;
        font-family: sans-serif;
        -webkit-font-smoothing: antialiased;
        font-size: 14px;
        line-height: 1.4;
        margin: 0;
        padding: 0;
        -ms-text-size-adjust: 100%;
        -webkit-text-size-adjust: 100%; 
      }

      table {
        border-collapse: separate;
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
        width: 100%; }
        table td {
          font-family: sans-serif;
          font-size: 14px;
          vertical-align: top; 
      }

      /* -------------------------------------
          BODY & CONTAINER
      ------------------------------------- */

      .body {
        background-color: #f6f6f6;
        width: 100%; 
      }

      /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
      .container {
        display: block;
        margin: 0 auto !important;
        /* makes it centered */
        max-width: 580px;
        padding: 10px;
        width: 580px; 
      }

      /* This should also be a block element, so that it will fill 100% of the .container */
      .content {
        box-sizing: border-box;
        display: block;
        margin: 0 auto;
        max-width: 580px;
        padding: 10px; 
      }

      /* -------------------------------------
          HEADER, FOOTER, MAIN
      ------------------------------------- */
      .main {
        background: #ffffff;
        border-radius: 3px;
        width: 100%; 
      }

      .wrapper {
        box-sizing: border-box;
        padding: 20px; 
      }

      .content-block {
        padding-bottom: 10px;
        padding-top: 10px;
      }

      .footer {
        clear: both;
        margin-top: 10px;
        text-align: center;
        width: 100%; 
      }
        .footer td,
        .footer p,
        .footer span,
        .footer a {
          color: #999999;
          font-size: 12px;
          text-align: center; 
      }

      /* -------------------------------------
          TYPOGRAPHY
      ------------------------------------- */
      h1,
      h2,
      h3,
      h4 {
        color: #000000;
        font-family: sans-serif;
        font-weight: 400;
        line-height: 1.4;
        margin: 0;
        margin-bottom: 30px; 
      }

      h1 {
        font-size: 35px;
        font-weight: 300;
        text-align: center;
        text-transform: capitalize; 
      }

      p,
      ul,
      ol {
        font-family: sans-serif;
        font-size: 14px;
        font-weight: normal;
        margin: 0;
        margin-bottom: 15px; 
      }
        p li,
        ul li,
        ol li {
          list-style-position: inside;
          margin-left: 5px; 
      }

      a {
        color: #3498db;
        text-decoration: underline; 
      }

      /* -------------------------------------
          BUTTONS
      ------------------------------------- */
      .btn {
        box-sizing: border-box;
        width: 100%; }
        .btn > tbody > tr > td {
          padding-bottom: 15px; }
        .btn table {
          width: auto; 
      }
        .btn table td {
        
          text-align: center; 
      }
        .btn a {
          background-color: #ffffff;
          border: solid 1px #3498db;
          border-radius: 5px;
          box-sizing: border-box;
          color: #3498db;
          cursor: pointer;
          display: inline-block;
          font-size: 14px;
          font-weight: bold;
          margin: 0;
          padding: 12px 25px;
          text-decoration: none;
          text-transform: capitalize; 
      }

      .btn-primary table td {
        background-color: white; 
      }

      .btn-primary a {
        background-color: #3498db;
        border-color: #3498db;
        color: #ffffff; 
      }

      /* -------------------------------------
          OTHER STYLES THAT MIGHT BE USEFUL
      ------------------------------------- */
      .last {
        margin-bottom: 0; 
      }

      .first {
        margin-top: 0; 
      }

      .align-center {
        text-align: center; 
      }

      .align-right {
        text-align: right; 
      }

      .align-left {
        text-align: left; 
      }

      .clear {
        clear: both; 
      }

      .mt0 {
        margin-top: 0; 
      }

      .mb0 {
        margin-bottom: 0; 
      }

      .preheader {
        color: transparent;
        display: none;
        height: 0;
        max-height: 0;
        max-width: 0;
        opacity: 0;
        overflow: hidden;
        mso-hide: all;
        visibility: hidden;
        width: 0; 
      }

      .powered-by a {
        text-decoration: none; 
      }

      hr {
        border: 0;
        border-bottom: 1px solid #f6f6f6;
        margin: 20px 0; 
      }

      /* -------------------------------------
          RESPONSIVE AND MOBILE FRIENDLY STYLES
      ------------------------------------- */
      @media only screen and (max-width: 620px) {
        table[class=body] h1 {
          font-size: 28px !important;
          margin-bottom: 10px !important; 
        }
        table[class=body] p,
        table[class=body] ul,
        table[class=body] ol,
        table[class=body] td,
        table[class=body] span,
        table[class=body] a {
          font-size: 16px !important; 
        }
        table[class=body] .wrapper,
        table[class=body] .article {
          padding: 10px !important; 
        }
        table[class=body] .content {
          padding: 0 !important; 
        }
        table[class=body] .container {
          padding: 0 !important;
          width: 100% !important; 
        }
        table[class=body] .main {
          border-left-width: 0 !important;
          border-radius: 0 !important;
          border-right-width: 0 !important; 
        }
        table[class=body] .btn table {
          width: 100% !important; 
        }
        table[class=body] .btn a {
          width: 100% !important; 
        }
        table[class=body] .img-responsive {
          height: auto !important;
          max-width: 100% !important;
          width: auto !important; 
        }
      }

      /* -------------------------------------
          PRESERVE THESE STYLES IN THE HEAD
      ------------------------------------- */
      @media all {
        .ExternalClass {
          width: 100%; 
        }
        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
          line-height: 100%; 
        }
        .apple-link a {
          color: inherit !important;
          font-family: inherit !important;
          font-size: inherit !important;
          font-weight: inherit !important;
          line-height: inherit !important;
          text-decoration: none !important; 
        }
        #MessageViewBody a {
          color: inherit;
          text-decoration: none;
          font-size: inherit;
          font-family: inherit;
          font-weight: inherit;
          line-height: inherit;
        }
        .btn-primary table td:hover {
          background-color: #1FC0D9 !important; 
        }
        .btn-primary a:hover {
          background-color: #1FC0D9 !important;
          border-color: #1FC0D9 !important; 
        } 
      }

    </style>
  </head>
  <body class="">
   
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body">
      <tr>
        <td>&nbsp;</td>
        <td class="container">
          <div class="content">

            <!-- START CENTERED WHITE CONTAINER -->
            <table role="presentation" class="main">
         
              <!-- START MAIN CONTENT AREA -->
              <tr>
                <td class="wrapper"> 
              
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                      <td>
                      <p>Fecha:'.$fecha.'&nbsp;&nbsp;Hora:'.$hora.'
                        <p>Hola&nbsp;'.$usuario.',</p>
                        <p>Se han encontrado irregularidades:</p>
                        <p>Seccion:'.$bloqueres.'</p>
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
                          <tbody>
                            <tr>
                              <td align="left">
                               <table role="presentation" border="1" cellpadding="0" cellspacing="0">
                                <thead>
                                        <tr>
                                            <th>PREGUNTA</th>
                                            <th>RESPUESTA</th>
                                            <th>COMENTARIO</th>
                                           
                                        </tr>
                                    </thead>
                                    
                                     <tbody>
                                     '.$contemail.'
                                     </tbody>
                               </table>
                               <br>
                               <br>
                               

                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <p>Porfavor atienda las irregularidades lo antes posible.</p>
                        <p>Gracias.</p>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>

            <!-- END MAIN CONTENT AREA -->
            </table>
            <!-- END CENTERED WHITE CONTAINER -->

            <!-- START FOOTER -->
            <div class="footer">
              <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td class="content-block">
                    <br>Alguna duda <a href="mailto:soporte@empresavirtual.com?Subject=Duda Sumapp">Contactarme</a>.
                  </td>
                </tr>
                <tr>
                  <td class="content-block powered-by">
                    Derechos reservados  Empresa virtual.
                  </td>
                </tr>
              </table>
            </div>
            <!-- END FOOTER -->

          </div>
        </td>
      
      </tr>
    </table>
  </body>
</html>');
$mail->WordWrap = 900000;

$mail->Send();
    
}

   
?>


	
	 
