
<?php include 'includes/header.php'; ?>
 
 <div id="page">   
<div class="header header-fixed header-logo-app">
        <a href="#" class="header-title">ALERTAS</a>
          <a href="#" class="header-icon header-icon-1" data-back-button><i class="fas fa-arrow-left"></i></a>
        <a href="#" class="header-icon header-icon-2" data-menu="menu-1"><i class="fas fa-bars"></i></a>
        <a href="mailto:soporte@sumapp.cloud?Subject=Tengo un problema" class="header-icon header-icon-3"><i class="fa fa-envelope"></i></a>
        <a href="#" class="header-icon header-icon-4" data-toggle-theme><i class="fas fa-moon"></i></a>
	</div>
	<?php include('includes/menu.php');?>
	<div class="page-content header-clear-large">	   
        
        <div class="profile-header">
            <div class="profile-left">
                <h1>
                    <?php echo $result->designation ?>
                    <div class="date">
                <span id="diaSemana" class="weekDay"></span>, 
                <span id="dia" class="day"></span> de
                <span id="mes" class="month"></span> del
                <span id="anio" class="year"></span>
            </div>
            <div class="clock">
                <span id="horas" class="hours"></span> :
                <span id="minutos" class="minutes"></span> :
                <span id="segundos" class="seconds"></span>
            </div>
                </h1>
    <?php      
  $sqlemails="SELECT * FROM tb_alertas WHERE cuenta='".$_SESSION['alogin']."' ";
$queryemails = $conexion->query($sqlemails);//Se ejecuta consulta
$arrayemails= array(); // Array donde vamos a guardar los datos 
while($resultadoemails = $queryemails->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arrayemails[]=$resultadoemails; // Guardar los resultados en la variable

} 

?>

<br>

                <div class="clear"></div>
            </div>
            <div class="profile-right">
                <a href="#">
       
                	  
                    
                </a>
            </div>
        </div>
        <center>
        

<h1 class="content">
 	<h2>Alertas</h2>
           Irregularidades
<p>	Los correos que se registren recibiran las irregularidades de los cuestionarios resueltos.</p>


        </h1>
        </center
        >
                             <script type="text/javascript">
 function actualizar(){location.reload(true);}

$( document ).ready(function() 
{
    
$("form#eliminar").submit(function(event)
{
    $(".capa").fadeIn(200);
    event.preventDefault();

    var formData = new FormData($(this)[0]);

    console.log(formData)
 
  $.ajax({
    url: 'acciones/eliminarcorreo.php',
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (returndata)
    {
        if(returndata == "2")
        { 
            alert("Error al guardar")
        } 
        else 
        {
            Swal.fire({
  title: '<p>EMAIL ELIMINADO</p>',
   text: 'empresavirtual.mx',
  icon: 'success',
    confirmButtonText:
    '<i class="fa fa-thumbs-up"></i> Aceptar'
})
   setInterval("actualizar()",1000);    
        }
        $(".capa").fadeOut(200);
    }
  });
 
  return false;
  });
  
});
</script>
<script type="text/javascript">
 function actualizar(){location.reload(true);}

$( document ).ready(function() 
{
    
$("form#data").submit(function(event)
{
    $(".capa").fadeIn(200);
    event.preventDefault();

    var formData = new FormData($(this)[0]);

    console.log(formData)
 
  $.ajax({
    url: 'acciones/capturarcorreo.php',
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (returndata)
    {
        if(returndata == "2")
        { 
            alert("Error al guardar")
        } 
        else 
        {
            Swal.fire({
  title: '<p>EMAIL REGISTRADO</p>',
   text: 'sumapp.cloud',
  icon: 'success',
    confirmButtonText:
    '<i class="fa fa-thumbs-up"></i> Aceptar'
})
   setInterval("actualizar()",1000);    
        }
        $(".capa").fadeOut(200);
    }
  });
 
  return false;
  });
  
});
</script>
        <form id="data">
        	<input type="hidden" value="<?php echo $_SESSION['alogin']  ?>" name="cuenta">
             <div class="input-style input-style-2 has-icon input-required">
                <i class="input-icon fa fa-user"></i>
                <span>Nombre</span>
                <em>(requerido)</em>
                <input type="name" name="nombre" required placeholder="">
            </div>        
             <div class="input-style input-style-2 has-icon input-required">
                <i class="input-icon fa fa-user"></i>
                <span>Apellidos</span>
                <em>(requerido)</em>
                <input type="name" name="apellidos" required placeholder="">
            </div>    
             <div class="input-style input-style-2 has-icon input-required">
                <i class="input-icon fas fa-envelope-open-text"></i>
                <span>Correo electronico</span>
                <em>(requerido)</em>
                <input type="email"  name="email" required placeholder="">
            </div>    
            <center>
<input type="submit" value="Registrar" name="submit" class="button button-m shadow-small button-circle bg-blue2-dark btnsubir">
</center>
            </form> 
        <div class="divider divider-margins"></div>
        
		<div class="content">
<div class="table-scroll">	
  <form class="cmxform" id="eliminar">		
            <table id="alertas" class="table-borders-dark">
                 <thead>
                <tr>
                    <th class="bg-highlight color-white">Nombre</th>
                    <th class="bg-highlight color-white">Apellidos</th>
                    <th class="bg-highlight color-white">Email</th>
                    <th  class="bg-highlight color-white">Eliminar</th>
                </tr>
                 </thead>
                 <tbody>
                <?php foreach ($arrayemails as $emails ):?>
                <tr>
                <td>  <input type="hidden" value="<?php  echo $emails->id_alerta ?>" name="id"><?php echo $emails->nombre ?></td>
                 <td><?php echo $emails->apellidos ?></td>
                 <td><?php echo $emails->email ?></td>
                 <td> <button class="button button-m shadow-small bg-red1-light " type="submit"> <i class="fas fa-user-times"></i></button></td>
                </tr>
                 <?php endforeach; ?>
             </tbody>
            </table>
            </form>	
        </div>
</div>

<?php include('includes/footer.php');?>
</body>
</html>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script type="text/javascript">
     var idioma=

            {

                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningun dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar dato especifico:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "ÃƒÅ¡ltimo",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                "buttons": {
                    "copyTitle": 'Informacion copiada',
                    "copyKeys": 'Use your keyboard or menu to select the copy command',
                    "copySuccess": {
                        "_": '%d filas copiadas al portapapeles',
                        "1": '1 fila copiada al portapapeles'
                    },

                    "pageLength": {
                    "_": "Mostrar %d filas",
                    "-1": "Todo"
                    }
                }
            };

    $(document).ready(function() {
    $('#alertas').DataTable( {
       "paging": true,
    "lengthChange": true,
    "searching": false,
    "ordering": true,
    "info": true,
    "autoWidth": true,
    "language": idioma,
    "lengthMenu": [[5,10,30,31, -1],[5,10,30,31,"Mostrar Todo"]]
    } );
} );
  $(function(){
  var actualizarHora = function(){
    var fecha = new Date(),
        hora = fecha.getHours(),
        minutos = fecha.getMinutes(),
        segundos = fecha.getSeconds(),
        diaSemana = fecha.getDay(),
        dia = fecha.getDate(),
        mes = fecha.getMonth(),
        anio = fecha.getFullYear(),
        ampm;
    
    var $pHoras = $("#horas"),
        $pSegundos = $("#segundos"),
        $pMinutos = $("#minutos"),
        $pAMPM = $("#ampm"),
        $pDiaSemana = $("#diaSemana"),
        $pDia = $("#dia"),
        $pMes = $("#mes"),
        $pAnio = $("#anio");
    var semana = ['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'];
    var meses = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
    
    $pDiaSemana.text(semana[diaSemana]);
    $pDia.text(dia);
    $pMes.text(meses[mes]);
    $pAnio.text(anio);
    if(hora>=12){
      hora = hora - 12;
      ampm = "PM";
    }else{
      ampm = "AM";
    }
    if(hora == 0){
      hora = 12;
    }
    if(hora<10){$pHoras.text("0"+hora)}else{$pHoras.text(hora)};
    if(minutos<10){$pMinutos.text("0"+minutos)}else{$pMinutos.text(minutos)};
    if(segundos<10){$pSegundos.text("0"+segundos)}else{$pSegundos.text(segundos)};
    $pAMPM.text(ampm);
    
  };
  
  
  actualizarHora();
  var intervalo = setInterval(actualizarHora,1000);
});
</script>
