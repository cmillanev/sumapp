
<?php include 'includes/header.php'; ?>
<div id="page">   
<div class="header header-fixed header-logo-app">
        <a href="#" class="header-title">CUESTIONARIOS</a>
        <a href="#" class="header-icon header-icon-1" data-back-button><i class="fas fa-arrow-left"></i></a>
        <a href="#" class="header-icon header-icon-2" data-menu="menu-1"><i class="fas fa-bars"></i></a>
        <a href="mailto:soporte@sumapp.cloud?Subject=Tengo un problema" class="header-icon header-icon-3"><i class="fa fa-envelope"></i></a>
        <a href="#" class="header-icon header-icon-4" data-toggle-theme><i class="fas fa-moon"></i></a>
	</div>
	<?php include('includes/menu.php');?>
	<?php 
   $sqlapp="SELECT id_app,id_sucursal,id_empresa FROM tb_usuario  WHERE correo='".$_SESSION['alogin']."'";
$queryapp = $conexion->query($sqlapp);//Se ejecuta consulta
$arrayapp= array(); // Array donde vamos a guardar los datos 
while($resultadoapp = $queryapp->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arrayapp[]=$resultadoapp; // Guardar los resultados en la variable

}
foreach ($arrayapp as $s) {
    $sucursal=$s->id_sucursal;
}
foreach ($arrayapp as $s) {
    $app=$s->id_app;
}
	 ?>


	 <?php 
     if ($sucursal!=0) {
     foreach ($arrayapp as $app ) : 
	 $sqlcuestionario="SELECT * FROM tb_encuesta WHERE id_app='".$app->id_app."'";
	  endforeach; 
$querycuestionario = $conexion->query($sqlcuestionario);//Se ejecuta consulta
$arraycuestionario= array(); // Array donde vamos a guardar los datos 
while($resultadocuestionario = $querycuestionario->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arraycuestionario[]=$resultadocuestionario; // Guardar los resultados en la variable

}

	  ?>
	   <!-- this page is required on pages you want to have snackbars-->
 
         

    <div class="page-content header-clear-medium">	    
        <p class="content">
            Seleccione un cuestionario para continuar
        </p>
    <div class="content">
            <div class="link-list link-list-1">
                <?php foreach ($arraycuestionario as $cuestionario ): ?>
                <a href="bloques.php?cuestionario=<?php echo $cuestionario->id_encuesta  ?>&sucursal=<?php echo $sucursal ?>" >
                    <i class="fas fa-th-large color-blue2-dark"></i>
                    <span><?php echo $cuestionario->c_nombre_encuesta ?></span>
                    <i class="fa fa-angle-right"></i>
                </a>
              <?php endforeach; ?>
            </div>
        </div>
	
<?php }else{ ?>
    <?php 
    foreach ($arrayapp as $s ) :   
    $sqlsucursales="SELECT * FROM tb_sucursal WHERE id_empresa='".$s->id_empresa."'";
endforeach;
$querysucursales = $conexion->query($sqlsucursales);//Se ejecuta consulta
$arraysucursales= array(); // Array donde vamos a guardar los datos 
while($resultadosucursales = $querysucursales->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arraysucursales[]=$resultadosucursales; // Guardar los resultados en la variable

}


 ?>
    <br>    
    <br>    
    <center><p class="content">
            Seleccione una sucursal
        </p></center>
        
    <div class="content">
             <div class="input-style input-style-2 input-required">
                <span>Selecciona un valor</span>
                <em><i class="fa fa-angle-down"></i></em>
                <select onchange="Obtenersucursal(this)">
                    <option value="default" disabled selected>Selecciona un valor</option>
                    <?php  foreach ($arraysucursales as $su ): ?>
                    <option value="<?php echo $su->id_sucursal ?>"><?php echo $su->sucursal ?></option>
                <?php   endforeach; ?>
                </select>
            </div>
        </div>
                    <script type="text/javascript">
   function Obtenersucursal(selectObject) {
    var sucursal = selectObject.value;  

  $.ajax({
        data:{'sucursal':sucursal}, // Adjuntamos los parametros
        url:'sucursal.php?app=<?php echo $app ?>', // ruta del archivo php que procesará nuestra solicitud
        type:  'get', // metodo por el cual se mandarán los datos
        beforeSend: function () { // callback que se ejecutará antes de enviar la solicitud
          console.log("Enviando por medio de post");
        },
        success:  function (response) { // callback que se ejecutará una vez el servidor responda
           $("#cuestionario").html(response);
        }
      });
    }


 </script>
 <div id="cuestionario"></div>
        <?php } ?>


<?php include('includes/footer.php');?>
</div>
</body>
</html>